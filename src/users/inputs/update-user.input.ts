import { Field, ID, InputType } from '@nestjs/graphql';
import { ObjectID, ObjectIdColumn } from 'typeorm';

@InputType()
export class UpdateUserInput {
  @Field(() => ID)
  @ObjectIdColumn()
  id: ObjectID;

  @Field({ nullable: true })
  email: string;

  @Field({ nullable: true })
  password: string;

  @Field({ nullable: true })
  name: string;

  @Field({ nullable: true })
  username: string;
}
