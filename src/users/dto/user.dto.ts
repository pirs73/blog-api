import { ObjectType, Field, ID } from '@nestjs/graphql';
import { ObjectID } from 'typeorm';
import { RoleTypes } from '../entities/user.entity';

@ObjectType() // decorators for graphql
export class UserType {
  @Field(() => ID)
  readonly id: ObjectID;

  @Field()
  readonly email: string;

  @Field()
  readonly password: string;

  @Field()
  readonly name: string;

  @Field()
  readonly username: string;

  @Field()
  readonly role: RoleTypes;

  @Field(() => Date)
  readonly createdAt: Date;

  @Field(() => Date)
  readonly updatedAt: Date;
}
