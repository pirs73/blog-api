import { Field, ID, ObjectType } from '@nestjs/graphql';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  ObjectID,
  ObjectIdColumn,
} from 'typeorm';

export enum RoleTypes {
  GUEST = 'guest',
  ADMIN = 'admin',
  MANAGER = 'manager',
}

@ObjectType()
@Entity('users')
export class UserEntity {
  @Field(() => ID)
  @ObjectIdColumn()
  id: ObjectID;

  @Field()
  @Column()
  email: string;

  @Field()
  @Column()
  password: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  name: string;

  @Field()
  @Column()
  username: string;

  @Field()
  @Column({
    type: 'enum',
    enum: RoleTypes,
    default: RoleTypes.GUEST,
  })
  role: RoleTypes;

  @Field()
  @Column()
  createdAt: Date;

  @Field()
  @Column()
  updatedAt: Date;

  @BeforeInsert()
  beforeInsertAction() {
    this.createdAt = new Date();
    this.updatedAt = new Date();
  }

  @BeforeUpdate()
  updateDate() {
    this.updatedAt = new Date();
  }
}
