import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        name: 'default',
        type: config.get<'mongodb'>('TYPEORM_CONNECTION'),
        url: config.get<string>(
          `mongodb://` +
            config.get<string>('TYPEORM_USERNAME') +
            ':' +
            config.get<string>('TYPEORM_PASSWORD') +
            '@' +
            config.get<string>('TYPEORM_HOST') +
            ':' +
            config.get<string>('TYPEORM_PORT') +
            '/' +
            config.get<string>('TYPEORM_DATABASE'),
        ),
        database: config.get<string>('TYPEORM_DATABASE'),
        entities: [__dirname + 'dist/**/*.entity{.js,.ts}'],
        useUnifiedTopology: true,
        useNewUrlParser: true,
        autoLoadEntities: true,
        retryDelay: 3000,
        retryAttempts: 10,
      }),
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: 'schema.gql',
      sortSchema: true,
      playground: true,
    }),
    UsersModule,
  ],
  providers: [],
})
export class AppModule {}
